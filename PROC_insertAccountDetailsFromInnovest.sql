SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------
--UPDATED BY PAVAN - ON 10082020
--GET CUSTOMER DETAILS BY USING ACCOUNT NUMBER FROM INNOVEST.
--    exec insertAccountDetailsFromInnovest '1000661' 
--------------------------------------------------------------------
CREATE PROCEDURE [dbo].[insertAccountDetailsFromInnovest]
@accountNumber varchar(20)=null

AS
BEGIN
    set @accountNumber = isnull(@accountNumber,0);
    declare @maxid int = 0,@result int = 0,@cpmid varchar(20)='';  

    select @maxid = isnull(cpm.ID,0) from customerProfileMaster cpm where cpm.accountNumber=@accountNumber;  
    if ( @maxid = 0 )
        BEGIN
            BEGIN TRY   
                select @maxid = isnull(Max(cpm.ID),0) from CustomerProfileMaster cpm;
                select @cpmid ='DUMMY'+cast(@maxid+1 as varchar); 
                

                    insert into CustomerProfileMaster (accountNumber,customerProfileId,
                            customerName, customerAddressLine1, customerAddressLine2,customerAddressLine3, customerAddressCity,customerAddressState,customerAddressZip,
                            phoneNumber,emailAddress,paymentMethod,accountStatus,createdDate,updatedDate)            
                    select top(1) @accountNumber, @cpmid, 
                    a.CustomerShortName , c.Address1 , c.Address2 , c.Address3 , c.City , c.State , c.ZipCode , '' , '' ,'CC',1 ,GETUTCDATE(),null
                    from devndtcodatahub.InnovestNDI.dbo.ContactAddresses c, devndtcodatahub.InnovestNDI.dbo.AccountMaster a,
                    devndtcodatahub.InnovestNDI.dbo.ContactAccountRoles r 
                    where a.CustomerAccountNumber=r.CustomerAccountNumber  and r.ContactID=c.ContactID and r.ContactID in 
                    (select ContactID from devndtcodatahub.InnovestNDI.dbo.ContactAccountRoles 
                    where CustomerAccountNumber=@accountNumber and ContactID!=1) 

                set @maxid=isnull(SCOPE_IDENTITY(),0);
            END TRY
            BEGIN CATCH
                select @result = -1*ERROR_NUMBER();
            END CATCH
        END

    
    return iif(@result < 0,@result,@maxid) ;
END




GO
