SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--=========================================================
--created by pavan on 12062020
--updateed by pavan on 15062020
--atuthor: prasad
-- inserting payment details of end user from authorize.net
--=============================================================

CREATE procedure [dbo].[insertPaymentDetailsByPaymentProfileId]
--declare
@paymentProfileId nvarchar(250),
@sequenceId int,
@batchSequenceId int = 0,
@excelSequenceId int = 0,
@paymentAmount float,
@transactionDate datetime = null,
@transactionType  varchar(50),
@responseCode  varchar(1),
@authCode  varchar(6),
@avsResultCode  varchar(1),
@cvvResultCode  varchar(1)=null,
@cavvResultCode varchar(1)=null,
@transId varchar(20),
@refTransID varchar(20)=null,
@networkTransId varchar(max) =null,
@transHash varchar(max)=null,
@cardLastFourDigits varchar(10),
@accountType varchar(20),
@messageCode varchar(6),
@messagedescription  varchar(max)=null,

@referenceNumber varchar(50)=null,
@remarks varchar(200)=null,
--@isPrimary int = null,
@result VARCHAR(20)   output,
@resultmsg varchar(50)  output

as
begin
set @result='-1';
set @resultmsg='Unknown error';
                                                               
OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  
		DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];

DECLARE @PPID varchar(250)='',@CPID varchar(20) ='',@CCTID int=0,@statusId int = 0;

-- check payment profile details in customerProfiledetial
if exists ( select *from  CustomerProfileDetail cpd where cpd.ID =@sequenceId )
begin
select @PPID =  CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID)))) 
from CustomerProfileDetail cpd where cpd.ID=@sequenceId;

select @CPID= isnull(cpd.customerProfileId ,'') from CustomerProfileDetail cpd where cpd.ID =@sequenceId;

if  ( @PPID = @paymentProfileId )
begin

set @result = @responseCode ;
set @resultmsg = @transactionType+ ' details added succesfull.';

BEGIN TRY
-- insert into CreditCardTransactions send succes
INSERT INTO [dbo].[CreditCardTransactions]
           ([customerProfileId]   ,[paymentProfileId_old]
           ,[paymentAmount]     ,[transactionDate]   ,[transactionType]   ,[responseCode]
           ,[authCode]        ,[avsResultCode]     ,[cvvResultCode]       ,[cavvResultCode]
           ,[transId]       ,[refTransID]       ,[transHash]           ,[cardLastFourDigits]
           ,[accountType]       ,[messageCode]
           ,[messagedescription]            ,[networkTransId]
           ,[profileReferenceId]            ,[createdDate]
           ,[referenceNumber] ,[remarks]
           )
     VALUES
(@CPID,
@paymentProfileId,
@paymentAmount,
GETUTCDATE() ,
@transactionType ,
@responseCode ,
@authCode ,
@avsResultCode,
@cvvResultCode,
@cavvResultCode,
@transId,
@refTransID,
@transHash,
@cardLastFourDigits,
@accountType,
@messageCode,
@messagedescription,
@networkTransId ,
@sequenceId
,GETUTCDATE()
,@referenceNumber 
,@remarks
 )

SET @CCTID=@@IDENTITY;
set @statusId = IIF(@responseCode=1,1,0);
exec insertExcelSequencesInBatchPayments @excelSequenceId,@batchSequenceId,@sequenceId,@statusId ,@CCTID;
   

--if(isnull(@isPrimary,0)=1)
--BEGIN
--update c set c.isPrimaryForPayment=0  where CustomerProfileDetail c where c.customerProfileId=@CPID and c.isPrimaryForPayment=1;
--update c set c.isPrimaryForPayment=1  where CustomerProfileDetail c where c.id=@sequenceId;
--end

END TRY
        BEGIN CATCH
				set @result =convert(varchar, ERROR_NUMBER()); 
                set @resultmsg=ERROR_MESSAGE(); 
		END CATCH;

end
ELSE
BEGIN
set @result ='0';
set @resultmsg = 'payment Proifle id not exist.'
END
END
 
else
begin
-- error message send
set @result ='0';
set @resultmsg = 'payment Proifle id not exist.'
  end  

select @result result, @resultmsg resultmsg;
end













GO
