SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
----------------------------------------------------------------------------
-- DROP procedure [dbo].[getBatchNamesFromBatchPayments]
-- GO
--===========================================================================
-- created by pavan on 03072020
-- batchWise  list
--   exec getBatchNamesFromBatchPayments '2020-06-01','2020-08-18','',10,2
--============================================================================
CREATE procedure [dbo].[getBatchNamesFromBatchPayments]
--declare
@FromDate varchar(30)='',
@ToDate varchar(30)='',
@getName varchar(50)=null,

@pageNumber int=1,
@pageSize int=50
as 
begin

    set @FromDate = iif(ISDATE(@FromDate)=1,@FromDate,'');
    set @ToDate = iif(ISDATE(@ToDate)=1,@ToDate,'');
 
    --if( isnull(@FromDate,'')+isnull(@ToDate,'')+isnull(@getName,'') != '' )
    --begin
       ;WITH Pg AS (
        select bnl.ID batchId,bnl.batchName batchName,bnl.createdDate createdDate,COUNT(*) OVER() totalRows
        from batchNameList bnl where bnl.batchName like '%'+isnull(@getName,'')+'%' --and  len(@getName)>0
            and convert(date,BNL.createdDate,111) BETWEEN 
            ( case when @FromDate = '' then convert(date,BNL.[createdDate],111) else convert(date,@FromDate,111) end)
                and 
            ( case when @ToDate = '' then convert(date,BNL.[createdDate],111) else   convert(date,@ToDate,111) end)
                 ORDER BY bnl.ID DESC
                 OFFSET @pageSize * (@pageNumber - 1) ROWS
                 FETCH NEXT @pageSize ROWS ONLY
                )

                select  *from pg 
                OPTION (RECOMPILE);
    --end
end





GO
