SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--===============================================================================================
-- create by pavan on 19082020			
--import single record in batch payments at the time of transaction insert in cct

--==============================================================================================
CREATE PROCEDURE [dbo].[insertExcelSequencesInBatchPayments]			
--declare			

@excelSequenceId int =0,
@batchSequenceId INT =0,
@profileReferenceId int =0,  --@sequenceId
@statusId int =0,
@transReferenceId int =0
---return @batchSequenceId
AS			
BEGIN	
    set NOCOUNT ON;
      --declare variables       
   
    declare  @accountReferenceId int =0,@refNo varchar(50)='';						
    declare @batch varchar(50)='',@amount float=0,@acNo varchar(50)='',@batchId int = 0;   
      

    select @batchId=bnl.ID, @batch= ISNULL(bnl.batchName,'') from batchNameList bnl 
     inner join ExcelBatchRecords e on e.batchId = bnl.ID where e.ID=@excelSequenceId;

    
    select   @acNo   =isnull(e.accountNumber ,''), 
             @refNo  =isnull( e.referenceNumber ,'') ,
             @amount =isnull(e.amount ,0) ,
             @batchSequenceId=iif(@batchSequenceId=0,  isnull(e.paymentReferenceId,0) ,@batchSequenceId)               
                    from ExcelBatchRecords e  where e.batchId =@batchId AND e.ID =@excelSequenceId;	 

    exec @accountReferenceId = insertAccountDetailsFromInnovest @acNo;

     if( @batchSequenceId = 0 ) 
        BEGIN  

    select @batchSequenceId = isnull(max(bp.ID),0) from batchPayments bp where bp.batchName = @batch and
                        bp.accountReferenceId = @accountReferenceId and bp.amount = @amount and bp.referenceNumber = @refNo;
              
        END
    if( @batchSequenceId = 0 ) 
        BEGIN  
            BEGIN TRY        
                 
           ------------------
            INSERT INTO [dbo].[batchPayments]	
            ([batchId],[batchName],[accountReferenceId],[profileReferenceId],[amount],[statusId],[transReferenceId],[createdDate],[referenceNumber] )
             VALUES  (@batchId ,@batch,@accountReferenceId,@profileReferenceId,@amount,@statusId,@transReferenceId,GETUTCDATE() ,@refNo )
                            
             
            set @batchSequenceId=isnull(SCOPE_IDENTITY(),0);

            update e set e.paymentReferenceId=@batchSequenceId,e.updatedDate = GETUTCDATE() from ExcelBatchRecords e where e.ID=@excelSequenceId;

            END TRY
            BEGIN CATCH
            select @batchSequenceId = isnull(max(bp.ID),0) from batchPayments bp where bp.batchName = @batch and
                        bp.accountReferenceId = @accountReferenceId and bp.amount = @amount and bp.referenceNumber = @refNo;
           
            END CATCH
        end 
            ELSE 
            BEGIN  
                UPDATE b set b.profileReferenceId=@profileReferenceId,
                b.statusId=@statusId,b.transReferenceId=@transReferenceId,b.updatedDate = GETUTCDATE() from batchPayments b where b.ID=@batchSequenceId and b.statusId = 0;
            end                     
      

UPDATE E SET E.recordStatus = @statusId,E.updatedDate=GETUTCDATE() FROM ExcelBatchRecords E
    INNER JOIN batchPayments b on b.ID=e.paymentReferenceId WHERE b.ID=@batchSequenceId ;
    

update bnl set bnl.TotalProcessed = bnl.TotalProcessed + @statusId,  
bnl.batchStatus = iif( (TotalRecords - TotalProcessed = 0),1,0 ),bnl.updatedDate=GETUTCDATE()
from batchNameList bnl
inner join batchPayments b on b.batchId=bnl.ID where b.ID=  @batchSequenceId;
       
end	


GO
