SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
CREATE TABLE [dbo].[DocuSignGeneratedProfiles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EnvelopeId] [varchar](150) NOT NULL,
	[accountNumber] [varchar](50) NOT NULL,
	[nameOnCard] [varchar](50) NULL,
	[cardLastFourDIgits] [varchar](10) NULL,
	[expiryDate] [varchar](10) NULL,
	[emailAddress] [varchar](50) NOT NULL,
	[DocuSignProfileStatus] [int] NULL,
	[profileReferenceId] [int] NULL,
	[accountReferenceId] [int] NULL,
	[recordStatus] [int] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[messageCode] [varchar](7) NULL,
	[messageText] [varchar](max) NULL,
 CONSTRAINT [PK_DGPID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
