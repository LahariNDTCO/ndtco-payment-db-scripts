SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--============================================
-- created by pavan on 03072020
-- batchWise  list
-- exec getBatchNamesPendingFromBatchNameList
--============================================
CREATE procedure [dbo].[getBatchNamesPendingFromBatchNameList]
--declare
@FromDate varchar(30)='',
@ToDate varchar(30)='',
@getName varchar(50)=''

as 
begin
    set @FromDate=iif(ISDATE(@FromDate)=1,@FromDate,'');
    set @ToDate=iif(ISDATE(@ToDate)=1,@ToDate,'');
 
    --UPDATE b set b.batchStatus=1 from batchNameList b inner join ExcelBatchRecords e on e.batchId=b.ID
    --where b.TotalRecords= ( select count(*) from ExcelBatchRecords ebr where ebr.batchId=e.batchId AND ebr.recordStatus=1 )  
    
    --UPDATE b set b.batchStatus=1 from batchNameList b inner join ExcelBatchRecords e on e.batchId=b.ID
    --    where b.TotalRecords= 
    --    ( select count(*) from batchPayments bp where bp.batchId=e.batchId AND bp.statusId=1 ) 
   
    select bnl.ID batchId,bnl.batchName batchName,bnl.createdDate createdDate
    from batchNameList bnl where bnl.batchName like '%' + @getName + '%' 
    and bnl.batchStatus = 0 and bnl.TotalRecords > 0 and convert(date,BNL.createdDate,111) BETWEEN 
        ( case when @FromDate = '' then convert(date,BNL.[createdDate],111) else convert(date,@FromDate,111) end)
        and 
        ( case when @ToDate = '' then convert(date,BNL.[createdDate],111) else   convert(date,@ToDate,111) end)
end



GO
