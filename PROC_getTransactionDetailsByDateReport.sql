SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
--==========================================================================================
--created by pavan on 2906  
--TRANSACTION DATA by date with param
--------------------------------------------------------------------------------------------------

CREATE Procedure [dbo].[getTransactionDetailsByDateReport]
--declare

@FromDate varchar(30)='',
@ToDate varchar(30)='',
@Status int=null,
@accountNumber varchar(30) =null,

@ttype varchar(50)=null,
--@Name VARCHAR(50)=null,

@pageNumber int=1,
@pageSize int=50

as 
begin

    set @FromDate = iif(ISDATE(@FromDate)=1,@FromDate,'');
    set @ToDate = iif(ISDATE(@ToDate)=1,@ToDate,'');

    if( ISNULL(@Status,-1) != -1  or ISNULL(@accountNumber,'') != ''  )
    begin  

        ;WITH Pg AS (
            select 
            cpm.customerName Name,
            cpm.accountNumber as accountNumber,

            s.statusText Status,
            cct.transactionDate Time_of_transaction,
            cct.transId transId,
            'NDTCO'  Merchent ,
            cct.paymentAmount Amount,

            (case  when cct.transactionType = 'authCaptureTransaction' then 'Charge'  else REPLACE(cct.transactionType,'Transaction','')  end  ) Code,
            cct.referenceNumber  referenceNumber,
            cct.responseCode,
            cct.transactionType ttype,
            COUNT(*) OVER() totalRows

            from CreditCardTransactions cct 
            INNER JOIN CustomerProfileDetail cpd on cpd.ID=cct.profileReferenceId
            INNER JOIN CustomerProfileMaster cpm on cpm.customerProfileId=cpd.customerProfileId
            left join StatusCodes s on s.statusCode=cct.responseCode
            where cpm.accountStatus=1 

            and convert(date,cct.transactionDate,111) BETWEEN 
            (case  when isnull(@FromDate,'')='' then convert(date,cct.[transactionDate],111)    else convert(date,@FromDate,111) end)
            and 
            (case when isnull(@ToDate,'')=''  then convert(date,cct.[transactionDate],111)   else convert(date,@ToDate,111) end)
            and cct.responseCode =(case when isnull(@Status,0) = 0 then cct.[responseCode] else @Status end)
            and cpm.accountNumber = (case when isnull(@accountNumber,'') = '' then cpm.[accountNumber] else @accountNumber end)
            --and ( @Name is  null or cpm.customerName like '%'+isnull( @Name,'')+'%')
            and ( cct.transactionType = (case when ISNULL(@ttype,'')='' then cct.[transactionType] else @ttype end ))
            ORDER BY cct.ID DESC
            OFFSET @pageSize * (@pageNumber - 1) ROWS
            FETCH NEXT @pageSize ROWS ONLY
        )

        select  *from pg 
        OPTION (RECOMPILE);

    end
end

GO
