SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--select *from batchPayments
--select *from creditCardTransactions
--exec getBatchPaymentsByBatchName 21
--===========================================================================================================			
-- create by pavan on 30062020			
--import Batch Transactions Details From Excel		
--apply begin try end try 	
-- if same refno came
-- if same record found
-- 
--==========================================================================================================			
			
CREATE PROCEDURE [dbo].[insertBatchPaymentDetailsFromExcel]			
--declare			
@json nvarchar(max)	,
@fileName varchar(50)		

--with execute as OWNER			
AS			
BEGIN	
set NOCOUNT ON;
declare @result int = -1 , @resultmsg varchar(50)= 'Unknown Error';		
 

declare @count int = 0,@scount int = 1,@insertCount int = 0,@checkAccounts varchar(max)='',@refNo varchar(50)='';			
declare @recordExistence int =0;			
declare @batchId int = 0,  @accountReferenceId int =0,@profileReferenceId int =0,@CPID varchar(50) = '0';			
declare @batch varchar(50)='',
@amount float=0,@acNo varchar(50)='';
declare @createdDate varchar(50)=substring(convert(varchar,getdate()),0,20);
select @batch = iif( @fileName = '' , 'Batch_' + CONVERT(VARCHAR,GETDATE(),112) , @fileName );	
select @batchId = isnull(max(bp.batchId),0)+1 from batchPayments bp;
select @batchId= ISNULL(bp.batchId,@batchId) from batchPayments bp where bp.batchName=@batch;	


DECLARE @batchDtls as table (scount int identity(1,1),  --batchName varchar(50), 
accountNumber varchar(50), refNo varchar(50), amount float  )			
			
declare @tbl as table ( accountNumber varchar(30), customerProfileId varchar(30),customerName varchar(50), Address1 varchar(40), Address2 varchar(40), Address3 varchar(40),
    City varchar(30), State varchar(2), ZipCode varchar(10), emailAddress varchar(50), phoneNumber varchar(15),
    PymentMethod varchar(2), accountStatus bit,  result varchar(2),  resultmsg varchar(50),  isProfileIdGenerated int )


IF ( ISJSON(@json) = 1 )			
begin			
    BEGIN TRY			
    insert into @batchDtls			
	select --batchName,
    accountNumber,refNo,amount from openjson(@json)		
	with		
	(		
		 
		--batchName varchar(50) '$.batchName',	
    	accountNumber varchar(50) '$.accountNumber', 		
    	refNo varchar(50) '$.referenceNumber',		
   		amount float '$.amount'  	
	)
    END TRY
    BEGIN CATCH
            select @result = ERROR_NUMBER();
            SELECT @resultmsg = ERROR_MESSAGE();
    END CATCH	
			
select @count =count(*) from @batchDtls;			
		
		
---------------------			
 BEGIN TRY	
    while(@scount<=@count)		
	begin		
		select @acNo=isnull(ltrim(rtrim(bd.accountNumber)) ,'') from @batchDtls bd where bd.scount =@scount;	
	    select @refNo=isnull(ltrim(rtrim(bd.refNo)) ,'') from @batchDtls bd where bd.scount =@scount;	
		
        select @amount=isnull(bd.amount ,0) from @batchDtls bd where bd.scount =@scount;	
		select @accountReferenceId = isnull( cpm.ID,0), @CPID = isnull( cpm.customerProfileId,'0')  from CustomerProfileMaster cpm where cpm.accountNumber =LTRIM(RTRIM(@acNo) );	
		
         if( @accountReferenceId = 0 and @acNo != '')
              BEGIN
              insert into @tbl 
              exec getCustomerDetailsByAccountNumber @acNo;
              select @accountReferenceId = isnull( cpm.ID,0), @CPID = isnull( cpm.customerProfileId,'0')  
              from CustomerProfileMaster cpm where cpm.accountNumber =@acNo;	
	          end
        
        select  @profileReferenceId=  ISNULL(MAX(c.ID),0) FROM CustomerProfileDetail c 
                                        where c.customerProfileId =@CPID and c.profileStatus=1;	
		select  @profileReferenceId= isnull(max(cct.profileReferenceId) ,@profileReferenceId) from CreditCardTransactions cct 	
                                        INNER join CustomerProfileDetail cpd on cpd.ID = cct.profileReferenceId 
                                        where cpd.customerProfileId =@CPID and cpd.profileStatus=1 
                                        and cct.responseCode=1;
		
         set @recordExistence = 0;
        ---------------- 			
		if(@accountReferenceId !=0 and @amount!=0 and (@refNo is not null))	
		begin 	  
           select @recordExistence = isnull(max(bp.ID),0) from batchPayments bp where --bp.batchName = @batch and
           bp.accountReferenceId = @accountReferenceId and bp.amount = @amount and bp.referenceNumber = @refNo;

          if( @recordExistence = 0 )           
          begin
            ------------------
		    INSERT INTO [dbo].[batchPayments]	
			   ([batchId]   ,[batchName]  ,[accountReferenceId]  ,[profileReferenceId]
                 ,[amount]    ,[statusId]   ,[transReferenceId]    ,[createdDate] , [referenceNumber] )
			
			 VALUES  (@batchId ,@batch,@accountReferenceId,@profileReferenceId,@amount,0,0,getdate(),@refNo )
			
		    set @insertCount=@insertCount + 1;	 
           
            end 
            ELSE 
            BEGIN             
            select @checkAccounts += iif(len(@checkAccounts)>0,', ','') + isnull(@acNo,'') +' (Amt: '+ convert(varchar,isnull(@amount,0))+',REf No:' +isnull(@refNo,'') +') Record already exists ';
            end                     
		end	        	
        ELSE 
          BEGIN          
           select @checkAccounts += iif(len(@checkAccounts)>0,', ','') + @acNo +' is invalid And Discarded,  ';
          END			
    	set @scount=@scount + 1;	
        		   
            
    	end		
	 END TRY
         BEGIN CATCH
            select @result = ERROR_NUMBER();
            SELECT @resultmsg = ERROR_MESSAGE();
         END CATCH  		
---------------------------	
--select @insertCount=isnull(count(b.ID),0) from batchPayments b where b.batchName=@batch;	 			
set @result = @batchId;			
set @resultmsg = 'Data Inserted for '+convert(varchar,@insertCount)+ ' Records';  
select @createdDate = substring(convert(varchar,isnull(max(b.createdDate),getdate())),0,20) from batchPayments b 
where b.ID=@batchId;   			
end			
else 			
begin			
set @result = 0;			
set @resultmsg = 'Invalid Json Format'; 			
end			

if(@insertCount = 0)
BEGIN
set @result = -2;			
set @resultmsg = 'Check the Data Once again.may be Duplicated file';   
end

select @result result,@resultmsg resultmsg	,@insertCount recordCount, @checkAccounts checkAccounts,@batch batchName,@createdDate createdDate	;	
end	

			






GO
