SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
--===========================================================================================================			
-- create by pavan on 30062020			
--import Batch Transactions Details From batchRecords		
--apply begin try end try 	
-- if same refno came
-- if same record found
-- select *from batchPayments where id=309
--==========================================================================================================			
			
CREATE PROCEDURE [dbo].[insertBatchPaymentDetailsFromBatchRecords]			
--declare			
@json nvarchar(max)	,    -- excelSequenceId
@batchId int=0,
@pageNumber int=1,
@pageSize int=50

--with execute as OWNER			
AS			
BEGIN	
    set NOCOUNT ON;

    OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY] DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];
    --declare variables        
    declare @result int = -1 , @resultmsg varchar(50)= 'Unknown Error';		 

    declare @count int = 0,@scount int = 1,@insertCount int = 0,@checkAccounts varchar(max)='',@refNo varchar(50)='';			
    declare @recordExistence int =0, @accountReferenceId int =0,@profileReferenceId int =0,@CPID varchar(50) = '0';			
    declare @batch varchar(50)='',@amount float=0,@acNo varchar(50)='';
    declare @createdDate varchar(50)=substring(convert(varchar,getdate()),0,20);
    declare @referenceId int=0,@excelSequence int =0, @batchSuccessCount int=0,@profileReferenceIdMax int = 0;

    DECLARE @batchDtls as table (scount int identity(1,1),excelSequenceId int) 
    --retrieve batch name
    select @batch= ISNULL(bnl.batchName,'') from batchNameList bnl where bnl.ID=@batchId;
    --process input parma json and insert in batch payments table
    IF ( ISJSON(@json) = 1 )			
    begin			
        BEGIN TRY			
            insert into @batchDtls select  excelSequenceId from openjson(@json) with (excelSequenceId int '$.excelSequenceId'	)
        END TRY
        BEGIN CATCH
            select @result = ERROR_NUMBER();
            SELECT @resultmsg = ERROR_MESSAGE();
        END CATCH	
                
        select @count =count(*) from @batchDtls;		
        
        BEGIN TRY	
            while(@scount<=@count)		
                begin	
                    select @excelSequence	= isnull(bd.excelSequenceId,0) from  @batchDtls bd where  bd.scount =@scount;	
                    
                    select   @acNo   =isnull(e.accountNumber ,''), @refNo  =isnull( e.referenceNumber ,'') , @amount =isnull(e.amount ,0)                 
                    from ExcelBatchRecords e  where e.batchId =@batchId AND e.ID =@excelSequence;	 

                    update  e set e.recordStatus = dbo.validateReference(e.referenceNumber)*7,e.updatedDate = GETUTCDATE() from ExcelBatchRecords e where e.batchId =@batchId 
                    AND e.ID =@excelSequence --and len(isnull(e.referenceNumber,''))=0;	   --ref no not given. 
                   
                    exec @accountReferenceId = insertAccountDetailsFromInnovest @acNo;
                    select  @CPID = isnull( cpm.customerProfileId,'0')  from CustomerProfileMaster cpm where cpm.accountNumber =@acNo;			
                
                    select  @profileReferenceIdMax= isnull(max(cct.profileReferenceId) ,0) from CreditCardTransactions cct 	
                                                    INNER join CustomerProfileDetail cpd on cpd.ID = cct.profileReferenceId 
                                                    where cpd.customerProfileId =@CPID and cpd.profileStatus=1 and cct.responseCode=1;
                    
                    select  @profileReferenceId=  ISNULL(MAX(c.ID), @profileReferenceIdMax) FROM CustomerProfileDetail c 
                                                    where c.customerProfileId =@CPID and c.profileStatus=1 and c.isPrimaryForPayment=1;	
                    
                
                    set @recordExistence = 0;     --param for check to see duplicate record
                    ---------------- 			
                    if(@accountReferenceId !=0 and @amount!=0 and (@refNo is not null) )	
                    begin 	  
                        select @recordExistence = isnull(max(bp.ID),0) from batchPayments bp where bp.batchName = @batch and
                        bp.accountReferenceId = @accountReferenceId and bp.amount = @amount and bp.referenceNumber = @refNo;

                        if( @recordExistence = 0 )           
                        begin          
                            ------------------
                            INSERT INTO [dbo].[batchPayments]	
                            ([batchId],[batchName],[accountReferenceId],[profileReferenceId],[amount],[statusId],[transReferenceId],[createdDate],[referenceNumber] )
                                    VALUES  (@batchId ,@batch,@accountReferenceId,@profileReferenceId,@amount,0,0,GETUTCDATE() ,@refNo )
                            
                            set @referenceId=@@IDENTITY;

                            update e set e.paymentReferenceId=@referenceId,e.updatedDate = GETUTCDATE() from ExcelBatchRecords e where e.ID=@excelSequence;
                            --set @insertCount=@insertCount + 1;
                        end 
                        ELSE 
                            BEGIN  
                                UPDATE b set b.profileReferenceId=@profileReferenceId,b.updatedDate = GETUTCDATE() from batchPayments b where b.ID=@recordExistence and b.statusId = 0;
                                --update e set e.recordStatus = 6 from ExcelBatchRecords e where e.ID=@excelSequence ; --duplcate record                              
                                select @checkAccounts += iif(len(@checkAccounts)>0,', ','') + isnull(@acNo,'') +' (Amt: '+ convert(varchar,isnull(@amount,0))+',REf No:' +isnull(@refNo,'') +') Record already exists ';
                            end                     
                    end	        	
                    ELSE 
                        BEGIN     
                            update e set e.recordStatus = 5,e.updatedDate = GETUTCDATE() from ExcelBatchRecords e where e.ID=@excelSequence and e.batchId =@batchId  ; --invalid account                     
                            select @checkAccounts += iif(len(@checkAccounts)>0,', ','') + @acNo +' is invalid And Discarded,  ';
                        END			
                    set @scount=@scount + 1;	
                    set @accountReferenceId=0;
                    set @profileReferenceId=0;
                    set @profileReferenceIdMax=0;    
                end		
        END TRY
        BEGIN CATCH
            select @result = ERROR_NUMBER();
            SELECT @resultmsg = ERROR_MESSAGE();
        END CATCH  		
        ---------------------------	
        select @insertCount=isnull(count(b.ID),0) from batchPayments b where b.batchName=@batch;	 			
        set @result = @batchId;			
        set @resultmsg = 'Data Inserted for '+convert(varchar,@insertCount)+ ' Records';  
        select @createdDate = substring(convert(varchar,isnull(max(b.createdDate),GETUTCDATE())),0,20) from batchPayments b 
        where b.ID=@batchId;   

    end			
    else 			
        begin			
            set @result = 0;			
            set @resultmsg = 'Invalid Json Format'; 			
        end			

    if(@insertCount = 0)
        BEGIN
            set @result = -2;			
            set @resultmsg = 'Check the Data Once again.may be Duplicated file';   
        end
        else 
            begin 
                BEGIN TRY
                    select @batchSuccessCount =  count(ebr.ID) from ExcelBatchRecords ebr where ebr.batchId=@batchId and ebr.recordStatus=1;
                    update bnl set bnl.batchStatus = iif(bnl.TotalRecords = @batchSuccessCount,1,0),bnl.updatedDate = GETUTCDATE() from batchNameList bnl where bnl.ID=@batchId;
                END TRY
                BEGIN CATCH
                    select @result = ERROR_NUMBER();
                    SELECT @resultmsg = ERROR_MESSAGE();
                END CATCH
            end

    select 
    e.batchId batchId,bnl.batchName batchName, isnull(e.accountNumber,'') accountNumber, 
    isnull(cpm.customerName,'') customerName,
    isnull(cpd.customerProfileId,'') customerProfileId,
    isnull(CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))),'') paymentProfileId,
    isnull(cpd.ID,0) sequenceId,  isnull(cpd.cardLastFourDigits,'') cardLastFourDigits,
    isnull(b.referenceNumber,'')  referenceNumber,isnull(b.amount,0) amount, b.statusId STATUS, isnull(cct.transId,0) transId,
    b.ID batchSequenceId, iif(b.profileReferenceId>0,1,0) isProfileGenerated,isnull(cct.responseCode,0) responseCode,
    isnull(cct.refTransID,0) refTransID, e.ID excelSequenceId,e.recordStatus recordStatus,sc.statusText statusText  
    from ExcelBatchRecords e    left join batchPayments b on b.ID=e.paymentReferenceId
                                inner join @batchDtls bd on bd.excelSequenceId=e.ID
                                inner join batchNameList bnl on bnl.ID=e.batchId
                                left join CustomerProfileMaster cpm on cpm.ID=b.accountReferenceId
                                left join CustomerProfileDetail cpd on cpd.ID=b.profileReferenceId
                                left join CreditCardTransactions cct on cct.ID=b.transReferenceId
                                left join statusCodes sc on sc.statusCode=e.recordStatus
                                where e.batchId = @batchId


    --select @result result,@resultmsg resultmsg	,@insertCount recordCount, @checkAccounts checkAccounts,@batch batchName,@createdDate createdDate	;	
end	






GO
