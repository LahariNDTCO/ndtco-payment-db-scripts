SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--=================================================================================
-- created by pavan on 21082020
-- insert failure status in docusign generated profiles table 

--======================================================================
CREATE procedure [dbo].[updateStatusInDocusignGeneratedProfiles]

--declare
@DocuSignSequenceId int,
@code varchar(10)=null,
@message varchar(max)=null
as 
BEGIN
    update d SET  d.recordStatus =10,d.DocuSignProfileStatus=3,
    d.messageCOde = isnull(@code,''), d.messageText = isnull(@message,'') 
    from DOcuSignGeneratedProfiles d
    where d.ID=@DocuSignSequenceId

end

GO
