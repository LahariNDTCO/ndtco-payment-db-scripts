SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerProfileDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[customerProfileId] [varchar](20) NOT NULL, 
	[cardLastFourDigits] [nvarchar](10) NULL,
	[cardExpiryDate] [varchar](10) NULL,
	[paymentProfileId_old] [nvarchar](250) NOT NULL,
	[nameOnCard] [varchar](50) NULL,
	[cardAddressLine1] [varchar](40) NULL,
	[cardAddressLine2] [varchar](40) NULL,
	[cardAddressState] [varchar](2) NULL,
	[cardAddressCity] [varchar](30) NULL,
	[cardAddressZip] [varchar](10) NULL,
	[phoneNumber] [varchar](15) NULL,
	[emailAddress] [varchar](50) NULL,
	[paymentProfileId] [varbinary](8000) NOT NULL,
	[profileStatus] [int] NULL,
	[isPrimaryForPayment] [int] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
 CONSTRAINT [PK_CPDID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [PK_CPDPPID_UNIQ] UNIQUE NONCLUSTERED 
(
	[paymentProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerProfileDetail]  WITH CHECK ADD  CONSTRAINT [FK_CPDCPID_CPMCPID] FOREIGN KEY([customerProfileId])
REFERENCES [dbo].[CustomerProfileMaster] ([customerProfileId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerProfileDetail] CHECK CONSTRAINT [FK_CPDCPID_CPMCPID]
GO
