SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCardTransactions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[customerProfileId] [varchar](20) NOT NULL,
	[paymentProfileId_old] [nvarchar](250) NOT NULL,
	[paymentAmount] [float] NULL,
	[transactionDate] [datetime] NULL,
	[transactionType] [varchar](50) NULL,
	[responseCode] [varchar](1) NULL,
	[authCode] [varchar](6) NULL,
	[avsResultCode] [varchar](1) NULL,
	[cvvResultCode] [varchar](1) NULL,
	[cavvResultCode] [varchar](1) NULL, 
	[transId] [nvarchar](20) NULL,
	[refTransID] [nvarchar](20) NULL,
	[transHash] [varchar](max) NULL,
	[cardLastFourDigits] [varchar](10) NULL,
	[accountType] [varchar](20) NULL,
	[messageCode] [varchar](6) NULL,
	[messagedescription] [varchar](max) NULL,
	[networkTransId] [varchar](max) NULL,
	[profileReferenceId] [int] NOT NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[referenceNumber] [varchar](30) NULL,
	[remarks] [varchar](200) NULL,
 CONSTRAINT [PK_CCTID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditCardTransactions]  WITH CHECK ADD  CONSTRAINT [FK_CCTCPID_CPMCPID] FOREIGN KEY([customerProfileId])
REFERENCES [dbo].[CustomerProfileMaster] ([customerProfileId])
GO
ALTER TABLE [dbo].[CreditCardTransactions] CHECK CONSTRAINT [FK_CCTCPID_CPMCPID]
GO
ALTER TABLE [dbo].[CreditCardTransactions]  WITH CHECK ADD  CONSTRAINT [FK_CCTPRID_CPDID] FOREIGN KEY([profileReferenceId])
REFERENCES [dbo].[CustomerProfileDetail] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CreditCardTransactions] CHECK CONSTRAINT [FK_CCTPRID_CPDID]
GO
