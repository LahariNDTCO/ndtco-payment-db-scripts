SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--====================================================================================
--author : prasad
-- created by pavan on 12062020
-- updated by pavan on 17062020
--proceudre for updating customer profile id and insert payment profile data
--===================================================================================
CREATE procedure [dbo].[InsertCustomerPaymentProfileDetailsByAccountNumber]
--DECLARE
@accountNumber varchar(20),
@customerProfileId varchar(20),
@cardLastFourDigits nvarchar(10),
@cardExpiryDate varchar(10),
@paymentProfileId nvarchar(250), 

@nameOnCard varchar(50) =null,
@cardAddressLine1 varchar(40)= null,
@cardAddressLine2 VARCHAR(40)= null,
@cardAddressState varchar(2) =null,
@cardAddressCity varchar(30)= null,
@cardAddressZip varchar(10)= null,
@phoneNumber VARCHAR(15)= null,
@emailAddress varchar(50) =null,

@isPrimary int =null,
@DocuSignSequenceId int =null

AS
begin

declare @result VARCHAR(20)='-1' , @resultmsg varchar(50)='' ;
                                                                
OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  
		DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];
DECLARE @sequenceId int=0;

if exists (select *from CustomerProfileMaster cpm where cpm.accountNumber = ltrim(rtrim(@accountNumber)))
BEGIN
BEGIN TRY
if exists( select *from CustomerProfileMaster cpm where cpm.accountNumber = ltrim(rtrim(@accountNumber)) and cpm.customerProfileId like 'DUMMY%' )
BEGIN

--UPDATE customerProfileId IN CUSTOMER PRFOFILE MASTER WITH VARIABLE @customerProfileId(AUTHORIZE.NET)
UPDATE CPM SET CPM.customerProfileId =@customerProfileId,CPM.updatedDate=GETUTCDATE()
FROM  CustomerProfileMaster CPM
where cpm.accountNumber = @accountNumber and cpm.customerProfileId like 'DUMMY%';
END

if exists(select *from CustomerProfileDetail cpd where cpd.customerProfileId=@customerProfileId and cpd.isPrimaryForPayment=1)
BEGIN
    if(isnull(@isPrimary,0)=1)
    BEGIN
    update cpd set cpd.isPrimaryForPayment=0,cpd.updatedDate=GETUTCDATE() from CustomerProfileDetail cpd 
    where cpd.customerProfileId=@customerProfileId and cpd.isPrimaryForPayment=1;
    END
        ELSE
        begin
        set @isPrimary=0;
        end
end
else 
BEGIN
set @isPrimary=1;
END

--INSERT CUSTOMER PAYMENT PROFILE DETAILS IN CustomerProfileDetail
INSERT INTO 
CustomerProfileDetail
( customerProfileId, cardLastFourDigits, cardExpiryDate, paymentProfileId_old,
  nameOnCard , cardAddressLine1 , cardAddressLine2 ,
  cardAddressState , cardAddressCity , cardAddressZip ,
  phoneNumber , emailAddress ,
  paymentProfileId ,profileStatus,isPrimaryForPayment,createdDate
)
VALUES
(@customerProfileId,  @cardLastFourDigits,  @cardExpiryDate ,  @paymentProfileId,
 @nameOnCard , @cardAddressLine1 , @cardAddressLine2 ,
@cardAddressState , @cardAddressCity , @cardAddressZip ,
@phoneNumber , @emailAddress ,
convert(varbinary,@paymentProfileId ),1,@isPrimary ,GETUTCDATE() )

SET @sequenceId = @@IDENTITY;
update CustomerProfileDetail 
SET paymentProfileId = EncryptByKey(Key_GUID('NDTCO_AUTHORIZE_SYMMETRIC_KEY'),paymentProfileId,1,HashBytes('SHA1',CONVERT(varbinary,ID)))
    where ID = @sequenceId and paymentProfileId = convert(varbinary,@paymentProfileId );

update  dgp set dgp.profileReferenceId = @sequenceId , dgp.recordStatus=1 ,dgp.DocuSignProfileStatus=1,
    dgp.updatedDate=GETUTCDATE() from  DocuSignGeneratedProfiles dgp where dgp.ID=isnull(@DocuSignSequenceId,0);

SET @result=convert(varchar, @sequenceId);
set @resultmsg='payment details added';
END TRY
BEGIN CATCH
SET @result=ERROR_NUMBER();
set @resultmsg=ERROR_MESSAGE();
end CATCH
end
else 
begin
set @result ='0';
set @resultmsg = 'account number not exist.';
END

select @result result ,@resultmsg resultmsg,isnull(@isPrimary,0) isPrimary
end




GO
