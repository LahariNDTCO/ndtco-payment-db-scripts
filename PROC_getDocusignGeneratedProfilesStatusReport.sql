SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO
--=================================================================================
-- created by pavan on 21082020
-- Report based on status docusign
-- exec getDocusignGeneratedProfilesStatusReport '9006462','2020-08-01','2020-08-20'
--======================================================================
CREATE procedure [dbo].[getDocusignGeneratedProfilesStatusReport]

@accountNumber  varchar(20) =null,
@FromDate varchar(20) = '',
@ToDate varchar(20) = ''

as 
BEGIN

    set @FromDate=iif(ISDATE(@FromDate)=1,@FromDate,'');
    set @ToDate =iif(ISDATE(@ToDate)=1,@ToDate,'');

        select d.accountNumber,    cpm.customerName,     isnull(sd.StatusText,'') Status,
        isnull(d.messageText,sr.StatusText) +' '+isnull(d.messagecode,'') Description, 
         d.createdDate, 
        d.emailAddress, isnull(cpd.cardLastFourDigits,'') cardLastFourDigits,   
        d.DocuSignProfileStatus,  
        d.recordStatus,d.ID DocuSignSequenceId

            from DocuSignGeneratedProfiles d
            left join StatusCodes sr on sr.statusCode=d.recordStatus
            left join StatusCodes sd on sd.statusCode=d.DocuSignProfileStatus
            left join CustomerProfileDetail cpd on cpd.ID=d.profileReferenceId
            left join CustomerProfileMaster cpm on cpm.accountNumber=d.accountNumber
            where 
            d.accountNumber = ( case when isnull(@accountNumber,'')='' then d.[accountNumber] else @accountNumber end )
            and  convert(date,d.createdDate,111) BETWEEN 
                (case  when @FromDate = '' then convert(date,d.[createdDate],111)    else convert(date,@FromDate,111) end)
                and 
                (case when @ToDate = ''  then convert(date,d.[createdDate],111)   else convert(date,@ToDate,111) end)
                            
end



GO
