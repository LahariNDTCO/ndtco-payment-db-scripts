SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
--================================================================================
-- authorised by prasad and created by pavan and created on 13072020
--procedure for uploading excel records without validations into ExcelBatchRecords.
--EXEC getExcelBatchRecordsPendingByBatchId 199
--==================================================================================
CREATE procedure [dbo].[getExcelBatchRecordsPendingByBatchId]
--declare			
@batchId int,
@status int=null,	
@pageNumber int=1,
@pageSize int=2147483647
--with execute as OWNER			
AS			
BEGIN	    

    set NOCOUNT ON;
    declare @result int = -1 , @resultmsg varchar(200)= 'Unknown Error';	 

    OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  
            DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];
    
    if exists  (select *from batchNameList bnl where bnl.ID=@batchId )
        BEGIN
            set @result  =@batchId ;
            select  @resultmsg = bnl.batchName from batchNameList bnl where bnl.ID=@batchId;
            set @resultmsg = @resultmsg +' Batch selected.';          

        ;WITH Pg AS (         
            select 
            e.batchId batchId,bnl.batchName batchName,
            isnull(e.accountNumber,'') accountNumber,
            isnull(cpm.customerName,'') customerName,
            isnull(cpm.customerProfileId,'') customerProfileId,
            isnull(CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))),'') paymentProfileId,
            isnull(cpd.ID,0) sequenceId,
            isnull(cpd.cardLastFourDigits,'') cardLastFourDigits,
            isnull(e.referenceNumber,'')  referenceNumber ,
            isnull(e.amount,0) amount,
            isnull(b.statusId,0) STATUS ,
            isnull(cct.transId,0) transId,
            isnull(b.ID,0) batchSequenceId,
            iif( isnull(cpd.ID,0)>0,1,0) isProfileGenerated,
            isnull(cct.responseCode,0) responseCode,
            isnull(cct.refTransID,0) refTransID,
            isnull(e.ID,0) excelSequenceId,
            isnull(e.recordStatus,0) recordStatus,
            sc.statusText statusText,  
            COUNT(*) OVER() totalRows
            from ExcelBatchRecords e
            left join batchPayments b on b.ID=e.paymentReferenceId

            inner join batchNameList bnl on bnl.ID=e.batchId
            left join CustomerProfileMaster cpm on cpm.accountNumber=e.accountNumber
            left join CustomerProfileDetail cpd on cpd.customerProfileId=cpm.customerProfileId and cpd.isPrimaryForPayment=1 and cpd.profileStatus=1
            left join CreditCardTransactions cct on cct.ID=b.transReferenceId
            left join statusCodes sc on sc.statusCode=e.recordStatus

            where e.batchId=@batchId 
            --and e.recordStatus =(case when @status is not null then @status else e.[recordStatus] end)
            AND  isnull(E.recordStatus,0) !=1
            and isnull(b.statusId,0)!=1
            ORDER BY e.ID  
            OFFSET @pageSize * (@pageNumber - 1) ROWS
            FETCH NEXT @pageSize ROWS ONLY
        )
        select *from pg 
        OPTION (RECOMPILE);

    end
    ELSE
    BEGIN
    set @result  = 0 ;
    set @resultmsg = 'File not exists.';	
    end

    select @result result,@resultmsg resultmsg;
     
end

GO
