SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
CREATE TABLE [dbo].[batchPayments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[batchId] [int] NULL,
	[batchName] [varchar](50) NULL,
	[accountReferenceId] [int] NULL,
	[profileReferenceId] [int] NULL,
	[amount] [float] NULL,
	[statusId] [int] NULL,
	[transReferenceId] [int] NULL,
	[createdDate] [datetime] NULL,
	[referenceNumber] [varchar](30) NULL,
	[updatedDate] [datetime] NULL,
	[invoiceReferenceId] [int] NULL,
 CONSTRAINT [PK_BPID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
