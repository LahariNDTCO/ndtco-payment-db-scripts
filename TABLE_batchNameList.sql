SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
CREATE TABLE [dbo].[batchNameList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[batchName] [varchar](150) NULL,
	[TotalRecords] [int] NULL,
	[batchStatus] [int] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[createdBy] [int] NULL,
	[updatedBy] [int] NULL,
	[TotalProcessed] [int] NULL,
 CONSTRAINT [PK_BNLID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
