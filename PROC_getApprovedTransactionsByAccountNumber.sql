SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
--==========================================================================================
--created by pavan on 29072020
--TRANSACTION getApprovedT ransactions By AccountNumber for Refund screen
--exec getApprovedTransactionsByAccountNumber '2020-07-01','2020-07-24','',null
--=+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=======

CREATE Procedure [dbo].[getApprovedTransactionsByAccountNumber]
--declare
@FromDate varchar(20)='',
@ToDate varchar(20)='',
@accountNumber varchar(30)='',
@transId varchar(30)='',
@Name VARCHAR(50)='',

  @pageNumber int=null,
  @pageSize int=null
as 
begin


set @FromDate=iif(ISDATE(@FromDate)=1,@FromDate,'');
set @ToDate =iif(ISDATE(@ToDate)=1,@ToDate,'');
 
    OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY] DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];  

    SET @pageSize = ISNULL(@pageSize, 2147483647) -- max int
    SET @pageNumber = ISNULL(@pageNumber, 1)

    ;WITH Pg AS (
        select 
        cpm.accountNumber, cpm.customerName, cpm.customerProfileId,
        CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))) paymentProfileId,
        cpd.nameOnCard, cpd.cardLastFourDigits, cpd.cardExpiryDate, cct.paymentAmount, cct.referenceNumber,
        cct.transId, cct.transactionDate, cct.responseCode, cpm.accountStatus, cct.refTransID, cct.transactionType,
        cpd.ID sequenceId
            from CreditCardTransactions cct 
                INNER JOIN CustomerProfileDetail cpd on cpd.ID=cct.profileReferenceId
                INNER JOIN CustomerProfileMaster cpm on cpm.customerProfileId=cpd.customerProfileId
                    where cpm.accountStatus=1 and cct.responseCode=1 
                    and cct.transactionType in ('authCaptureTransaction','priorAuthCaptureTransaction')
                    and cct.transId not in
                    (select refTransID from CreditCardTransactions c where c.responseCode in(1,4) and c.ID=cct.ID)
                    AND cpm.accountNumber=(case when @accountNumber != '' then @accountNumber else cpm.[accountNumber] end)
                    AND cct.transId=(case when  @transId != '' then @transId else cct.[transId] END)
                    and  ( @Name is  null or cpm.customerName like '%'+ @Name+'%') and
                    convert(date,cct.transactionDate,111) BETWEEN 
                        (case  when @FromDate = '' then convert(date,cct.[transactionDate],111)    else convert(date,@FromDate,111) end)
                        and 
                        (case when @ToDate = ''  then convert(date,cct.[transactionDate],111)   else convert(date,@ToDate,111) end)
                    --and isnull(@accountNumber,'')+isnull(@transId,'')+isnull(@FromDate ,'')+isnull(@ToDate,'') != ''
                    and DATEDIFF(HOUR,cct.TransactionDate,getDate()) >24   
                        ORDER BY cct.ID DESC OFFSET @pageSize * (@pageNumber - 1) ROWS FETCH NEXT @pageSize ROWS ONLY
    )
    select *from pg 
    OPTION (RECOMPILE);
end





GO
