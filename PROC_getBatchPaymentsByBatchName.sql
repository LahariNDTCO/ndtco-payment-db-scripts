SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--=======================================================

-- created by pavan on 03072020
-- get batchWise payments records  list based on batch id.
--if status 0-failed list,1-success list,2-both list
--=========================================================
--exec getBatchPaymentsByBatchName 8
--select *from batchPayments
--========================================================

CREATE procedure [dbo].[getBatchPaymentsByBatchName]
--declare
  @batchId int =null,

  @pageNumber int=1,
  @pageSize int=10,
   @status int=null
 
as 
begin

    OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];

    ;WITH Pg AS (
        select b.batchId batchId,b.batchName batchName,isnull(cpm.accountNumber,'') accountNumber,
        isnull(cpm.customerName,'') customerName,
       --isnull(cpd.customerProfileId,'') customerProfileId,
       -- isnull(CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))),'') paymentProfileId,
        cpd.ID sequenceId, cpd.cardLastFourDigits cardLastFourDigits, isnull(b.referenceNumber,'')  referenceNumber ,
        isnull(b.amount,0) amount,b.statusId STATUS ,isnull(cct.transId,0) transId,
       -- b.ID batchSequenceId,iif(b.profileReferenceId>0,1,0) isProfileGenerated ,
        isnull(cct.responseCode,0) responseCode,isnull(cct.refTransID,0) refTransID
            from batchPayments b
            inner join CustomerProfileMaster cpm on cpm.ID=b.accountReferenceId
            left join CustomerProfileDetail cpd on cpd.ID=b.profileReferenceId
            left join CreditCardTransactions cct on cct.ID=b.transReferenceId
                where b.batchId =(case when @batchId is not null then @batchId else b.[batchId]  end) 
                and b.statusId= (case when @status is null then b.[statusId] else @status end )
                --and b.createdDate BETWEEN @FromDate and @ToDate
                ORDER BY b.ID   OFFSET @pageSize * (@pageNumber - 1) ROWS  FETCH NEXT @pageSize ROWS ONLY
    )
    select *from pg 
    OPTION (RECOMPILE);

end


GO
