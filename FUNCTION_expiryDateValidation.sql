SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[expiryDateValidation] ( @S VARCHAR(4000),@D VARCHAR(1) )
RETURNS int 
AS
begin
    DECLARE  @expValidate int;
    declare @cy int = datepart(year,getdate())-convert(int,substring( convert(varchar,datepart(year,getdate())),3,2));
    DECLARE  @x int=1;
    declare @dx int =ascii(@D);
    declare @count int = 0; select @count= count(*) from  dbo.[SplitString](@S,@D) ;
    WHILE @x<=len(@S) and @x > 0
        BEGIN
            set @x= iif( ascii(SUBSTRING(@S,@x,1)) in( @dx,48,49,50,51,52,53,54,55,56,57),(@x+1)*1, -1 )
        END
    if(@x>0 and len(@S) <= 7 and @count=2 )
        begin
            DECLARE @m int=0; select @m= convert(int,s.Val) from dbo.[SplitString](@S,@D) s where s.Id=1; set @m=iif(@m between 1 and 12,@m,0);
            DECLARE @y int=0; select @y= iif(len(s.Val)=4,convert(int,s.Val),iif(len(s.Val)=2,convert(int,s.Val)+@cy ,0)) from dbo.[SplitString](@S,@D) s where s.Id=2; set @y=iif(@y between 1 and 9999,@y,0);
            if(@m*@y=0)
            begin
                set @expValidate=-1;
            end
            else
                begin
                   set @expValidate = DATEDIFF(DAY,GETDATE(),convert(date, concat(@y ,'-',@m,'-',iif( @m in(1,3,5,7,8,10,12),31, iif( @m in(4,6,9,11),30,iif(@y%4=0 and @m=2,29,28)))),111));
                end
        end
        else 
            begin
                set @expValidate=-1;
            end

    return iif(@expValidate>=0,1,0)
end

 



GO
