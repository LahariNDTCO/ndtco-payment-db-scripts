SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--================================================================================
-- authorised by prasad and created by pavan and created on 13072020
--procedure for uploading excel records without validations into ExcelBatchRecords.

--==================================================================================

CREATE procedure [dbo].[insertExcelRecordsDump]
--declare			
@excelJson nvarchar(max),
@fileName varchar(50),	
 @pageNumber int=null,
  @pageSize int=null
--with execute as OWNER			
AS			
BEGIN	

     set    @pageNumber =isnull(@pageNumber,1);
     set    @pageSize =isnull(@pageSize,2147483647)    --max int

    set NOCOUNT ON;
    declare @result int = -1 , @resultmsg varchar(200)= 'Unknown Error';	
    declare @newBatchId INT = 0;

    OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  
		DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];

    IF ( ISJSON(@excelJson) = 1 )			
        begin	
            set @result  = 1 ;
            set @resultmsg = 'Records uploaded.';
            select @newBatchId= isnull(bnl.ID,0) from batchNameList bnl where bnl.batchName = REPLACE(@fileName,'.xlsx','') ;

            if  ( @newBatchId = 0 )        
                BEGIN
                    BEGIN TRY
                    INSERT INTO [dbo].[batchNameList] (  [batchName] ,[TotalRecords],[batchStatus],[createdDate],[TotalProcessed] )
                                                VALUES ( REPLACE(@fileName,'.xlsx','') ,0 ,0, GETUTCDATE(),0 )
                    set @newBatchId=@@IDENTITY;                          
                    end TRY
                    BEGIN CATCH
                    SET @result = ERROR_NUMBER();
                    SET @resultmsg = ERROR_MESSAGE();	
                    END CATCH
                END
   		        ---	
                BEGIN TRY			
                    insert into ExcelBatchRecords			
                        select @newBatchId,
                        accountNumber,referenceNumber,amount,0,null,GETUTCDATE(),null,balanceAmount,totalAmount from openjson(@excelJson) 	
                        with		
                        (	              
                            accountNumber varchar(50) '$.accountNumber', 		
                            referenceNumber varchar(50) '$.referenceNumber',		
                            amount float '$.amount', 
                            balanceAmount float '$.balanceAmount',
                            totalAmount float '$.totalAmount'                                 

                        ) jsn where jsn.referenceNumber NOT IN ( select e.referenceNumber from ExcelBatchRecords e where 
                                                                    --e.batchId=@newBatchId and e.totalAmount = jsn.totalAmount
                                                                   e.balanceAmount <= jsn.balanceAmount and e.referenceNumber=jsn.referenceNumber )
                                                                    and jsn.balanceAmount > 0 and jsn.totalAmount >= jsn.balanceAmount and jsn.balanceAmount >= jsn.amount
                        
                    UPDATE bnl set bnl.TotalRecords =(select count(EBR.ID) from ExcelBatchRecords EBR where EBR.batchId = @newBatchId)
                        from batchNameList bnl where bnl.ID=@newBatchId; 

                    UPDATE bnl set bnl.batchStatus = 0,bnl.updatedDate=GETUTCDATE()  from batchNameList bnl where bnl.ID=@newBatchId
                        and bnl.TotalRecords != (select count(EBR.ID) from ExcelBatchRecords EBR where EBR.batchId = @newBatchId and EBR.recordStatus = 1 )
                    
                END TRY
                BEGIN CATCH
                        select @result = ERROR_NUMBER();
                        SELECT @resultmsg = ERROR_MESSAGE();
                END CATCH	      
      
        END
       
    else 
        BEGIN
            set @result  = 0 ;
            set @resultmsg = ' Invalid Json Format.';	
        END



    ;WITH Pg AS ( 
    select ebr.batchId,ebr.accountNumber accountNumber,ebr.referenceNumber referenceNumber,ebr.amount amount,
    ebr.recordStatus recordStatus,ebr.paymentReferenceId paymentReferenceId,ebr.ID excelSequenceId,sc.statusText statusText,
    @result result,@resultmsg resultmsg
    from ExcelBatchRecords ebr 
    left  join statusCodes sc on sc.statusCode = isnull(ebr.recordStatus,0) 
    where ebr.batchId=@newBatchId 
    ORDER BY ebr.ID  
    OFFSET @pageSize * (@pageNumber - 1) ROWS
    FETCH NEXT @pageSize ROWS ONLY
    )

    select *from pg 
    OPTION (RECOMPILE);
    select @result result,@resultmsg resultmsg;

    -- ;WITH Pg AS ( 
    -- select ebr.batchId,ebr.accountNumber,ebr.referenceNumber,ebr.amount,
    -- isnull(cpd.customerProfileId,'') customerProfileId,
    --  isnull(CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))),'') paymentProfileId,
    --  cpd.ID sequenceId,
    --  cpd.cardLastFourDigits cardLastFourDigits,
    -- b.statusId STATUS ,
    -- isnull(cct.transId,0) transId,
    -- b.ID batchSequenceId,
    -- iif(b.profileReferenceId>0,1,0) isProfileGenerated ,
    -- isnull(cct.responseCode,0) responseCode,
    -- isnull(cct.refTransID,0) refTransID
    -- from ExcelBatchRecords ebr 
    -- left join batchPayments b on ebr.paymentReferenceId =b.ID
    -- left join CustomerProfileDetail cpd on cpd.ID=b.profileReferenceId
    -- left join creditCardTransactions cct on cct.id=b.transReferenceId
    -- where ebr.batchId=@newBatchId 
    -- ORDER BY ebr.ID  
    -- OFFSET @pageSize * (@pageNumber - 1) ROWS
    -- FETCH NEXT @pageSize ROWS ONLY
    -- )
 
end







GO
