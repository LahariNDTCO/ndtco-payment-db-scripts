SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================== ======================================
--create by pavan on 18062020
--update customer details using acountnumber
--========================================================

CREATE PROCEDURE [dbo].[setCustomerDetailsByAccountNumber]

@accountNumber varchar(20),

@customerAddressLine1 varchar(40)=null,
@customerAddressLine2 varchar(40)=null,
@customerAddressLine3 varchar(40)=null,
@customerAddressCity varchar(30)=null,
@customerAddressState varchar(2)=null,
@customerAddressZip varchar(10)=null,
@customerEmailAddress varchar(30)=null,
@customerPhoneNumber varchar(15)=null,

@result varchar(2) output,
@resultmsg VARCHAR(50)  output
AS
BEGIN

if exists( select *from  CustomerProfileMaster cpm where cpm.accountNumber =ltrim(rtrim(@accountNumber)) and cpm.accountStatus=1  )
begin
update cpm set    
    cpm.customerAddressLine1 = @customerAddressLine1,
    cpm.customerAddressLine2 = @customerAddressLine2,
    cpm.customerAddressLine3 = @customerAddressLine3,
    cpm.customerAddressCity = @customerAddressCity,
    cpm.customerAddressState = @customerAddressState,
    cpm.customerAddressZip = @customerAddressZip,
    cpm.emailAddress = @customerEmailAddress,
    cpm.phoneNumber = @customerPhoneNumber
     
     from CustomerProfileMaster cpm
    where cpm.accountNumber =ltrim(rtrim(@accountNumber))  ;
    set @result='1';
    set @resultmsg='Customer Details updated successfully.';
END
else
BEGIN
set @result='0';
    set @resultmsg='Customer Details cannot be updated.';
end

select @result result,@resultmsg resultmsg;

end 

GO
