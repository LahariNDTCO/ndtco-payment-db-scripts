SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
-- =============================================
-- Author: prasad
--created by : pavan Create date: 11/06/2020 -- update on 27-07-2020
-- Description: fetch card details based on customer profile id(PaymentProfiledata)
--exec getCustomerPaymentProfiledetailsByAccountNumber '1000000'
-- =============================================
CREATE PROCEDURE [dbo].[getCustomerPaymentProfiledetailsByAccountNumber]
--DECLARE
@accountNumber varchar(20) 
AS
BEGIN

OPEN SYMMETRIC KEY [NDTCO_AUTHORIZE_SYMMETRIC_KEY]  
		DECRYPTION BY CERTIFICATE [NDTCO_AUTHORIZE_CERTIFY];

 update cpd set cpd.profileStatus = dbo.[expiryDateValidation](cpd.cardExpiryDate,'/') 
 from   CustomerProfileDetail cpd 
 inner join CustomerProfileMaster cpm on cpm.customerProfileId=cpd.customerProfileId
  where cpm.accountNumber =@accountNumber

SELECT cpd.cardLastFourDigits
      ,cpd.cardExpiryDate
      , CONVERT(NVARCHAR,DecryptByKey(cpd.PaymentProfileID, 1 , HashBytes('SHA1', CONVERT(varbinary, cpd.ID) ))) paymentProfileId
     
      ,cpd.customerProfileId
      ,cpd.ID sequenceId
      ,cpd.isPrimaryForPayment isPrimary
      ,cpd.profileStatus profileStatus
  FROM CustomerProfileDetail cpd
  inner join CustomerProfileMaster cpm on cpm.customerProfileId=cpd.customerProfileId
  where cpm.accountNumber =@accountNumber
        and cpm.customerProfileId not like '%DUMMY%'
        and cpd.profileStatus=1;

     END


GO
