SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--============================== =============================================================================			
-- create by pavan on 30062020	,updated on 12082020		
--insertDocuSignRecordsForProfileGenerations	
--==========================================================================================================			
			
CREATE PROCEDURE [dbo].[insertDocuSignRecordsForProfileGenerations]			
--declare			
@json nvarchar(max)	 
AS			
BEGIN	
    set NOCOUNT ON;
    set @json = REPLACE(@json,' ','');
    --declare varaiables
    declare @result int = -1 , @resultmsg varchar(max)= '';		

    declare @DocusignRecords as table 
    (  scount int identity(1,1), EnvelopeId varchar(150) , accountNumber [varchar](50), nameOnCard [varchar](50),   emailAddress [varchar](50))    
    declare @count int = 0,@scount int = 1,@insertCount int=0,	@recordStatus int=0;	
    declare @accountReferenceId int =0, @profileReferenceId int =0, @CPID varchar(50) = '0';	

    declare @acNo varchar(50)='', @nameOnCard varchar(40) ='',@EnvelopeId varchar(250)='',@email varchar(50)='';  
    declare @DStatus int = 0;
    --process json data
    IF ( ISJSON(@json) = 1 )			
        begin			
            --convert json to  table 
            insert into @DocusignRecords 
            select  EnvelopeId ,accountNumber,nameOnCard, emailAddress   from openjson(@json) 
            with		
            (	 EnvelopeId varchar(150)  '$.envelopeId',	
                accountNumber [varchar](50)  '$.accountNumber',	
                nameOnCard [varchar](50) '$.nameOnCard',	    
                emailAddress [varchar](50)  '$.email'	
            
            );
            
            select @count =count(*) from @DocusignRecords;					

            --Validate each record with DocuSignGeneratedProfiles table
            a:  while(@scount<=@count)		
            begin		
                select @acNo=isnull(dr.accountNumber ,''), @nameOnCard=dr.nameOnCard, @EnvelopeId=dr.EnvelopeId, @email=dr.emailAddress 
                from @DocusignRecords dr where dr.scount =@scount;   

                --retrieve account number refernce from master table
                exec @accountReferenceId=insertAccountDetailsFromInnovest  @acNo;    
                
                --check paymentprofile exist or not
                select  @profileReferenceId =  ISNULL(max(c.ID),0) FROM CustomerProfileDetail c 
                inner join CustomerProfileMaster cpm on c.customerProfileId=cpm.customerProfileId  
                where cpm.accountNumber=@acNo;
            
                if not exists( select dgp.ID from DocuSignGeneratedProfiles dgp where dgp.EnvelopeId = @EnvelopeId)
                    begin
                        set @recordStatus=0;
                    end
                    ELSE
                        begin
                            select @recordStatus = dgp.ID from DocuSignGeneratedProfiles dgp where dgp.EnvelopeId = @EnvelopeId;
                        END
                    --update d set d.recordStatus = 9 from DocuSignGeneratedProfiles d where d.ID=@recordStatus and d.DocuSignProfileStatus=0 and d.recordStatus=0;
                --insert validated records in  DocuSignGeneratedProfiles table
                set @DStatus = iif( @profileReferenceId=0,0,9 );
                set @DStatus = iif( @accountReferenceId=0,5,0 );
                 update d set d.recordStatus = @DStatus,d.DocuSignProfileStatus= iif( @accountReferenceId=0,2,0 ) from DocuSignGeneratedProfiles d where d.ID=@recordStatus and d.DocuSignProfileStatus=0 and d.recordStatus=0;
                

                if( @profileReferenceId = 0 and  @recordStatus = 0 )              
                    begin
                        begin try
                            insert into DocuSignGeneratedProfiles
                            (
                                EnvelopeId, accountNumber, nameOnCard, emailAddress,  DocuSignprofileStatus,   profileReferenceId,  
                                accountReferenceId, recordStatus, createdDate,  updatedDate
                            )
                            VALUES ( @EnvelopeId, @acNo, @nameOnCard, @email, iif( @accountReferenceId=0,2,0 ),0,@accountReferenceId, @DStatus , GETUTCDATE(), NULL   )
                        

                        end TRY 
                        BEGIN CATCH
                        select @result = ERROR_NUMBER();
                        SELECT @resultmsg = @resultmsg+ ' Envelope '+@EnvelopeId +'('+@acNo+') not processed. ' + ERROR_MESSAGE();
                        set @scount=@scount + 1;	
                        GOTO a;
                        end CATCH	
                    END
                    
                set @profileReferenceId=0;
                set @recordStatus=0;   
                set @accountReferenceId=0;
                set @scount=@scount + 1;	
            
            end		
                
            -- data insert completed, set result param
            set @result = 1;			
            set @resultmsg =@resultmsg + ' Data Inserted';     			
        end			
        else 			
            begin			
                set @result = 0;			
                set @resultmsg = ' Invalid Json Format '; 			
            end		

    select dgp.EnvelopeId, dgp.accountNumber, isnull(cpm.customerProfileId,'') customerProfileId,
    dgp.ID DocuSignSequenceId,  iif(dgp.profileReferenceId>0,1,0) isProfileGenerated,
    dgp.recordStatus recordStatus,dr.emailAddress emailAddress, sc.statusText statusText,@result result, @resultmsg resultmsg ,
    dgp.createdDate createdDate
    from DocuSignGeneratedProfiles dgp 
    inner join @DocusignRecords dr on dr.EnvelopeId=dgp.EnvelopeId
    inner join CustomerProfileMaster cpm on cpm.ID=dgp.accountReferenceId
    left join CustomerProfileDetail cpd on cpd.customerProfileId=cpm.customerProfileId
    left join statusCodes sc on sc.statusCode=dgp.recordStatus
    WHERE isnull(dgp.profileReferenceId,0)=0 and  isnull(cpd.ID,0)=0
    and dgp.DocuSignProfileStatus=0 and dgp.recordStatus=0
	order by dgp.createdDate
    select @result result, @resultmsg resultmsg ;
    
end		






GO
