SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
-------------------------------------------------------------------
--UPDATED BY PAVAN - ON 10082020
--GET CUSTOMER DETAILS BY USING ACCOUNT NUMBER.

--------------------------------------------------------------------

CREATE PROCEDURE [dbo].[getCustomerDetailsByAccountNumber]
--declare 
@accountNumber varchar(20)
AS
BEGIN
    declare @result varchar(2)='1' , @resultmsg varchar(50)='Record Exists' ;

    declare @accountReferenceId int =0;
    exec @accountReferenceId = insertAccountDetailsFromInnovest @accountNumber;

    declare @isProfileIdGenerated int = 0;
    select @isProfileIdGenerated = iif(count(cpm.ID) > 0,0,1) from [CustomerProfileMaster] cpm where cpm.ID =@accountReferenceId and cpm.customerProfileId like 'DUMMY%' ;
    
    select cpm.accountNumber accountNumber, cpm.customerProfileId customerProfileId, cpm.customerName customerName,
        cpm.customerAddressLine1 Address1, cpm.customerAddressLine2 Address2,
        cpm.customerAddressLine3 Address3, cpm.customerAddressCity City,  cpm.customerAddressState State,
         cpm.customerAddressZip ZipCode,  cpm.emailAddress emailAddress, cpm.phoneNumber phoneNumber,    cpm.accountStatus accountStatus,
        @result result,    @resultmsg resultmsg,  @isProfileIdGenerated isProfileIdGenerated
     from CustomerProfileMaster cpm   where cpm.ID = @accountReferenceId                                       
END


GO
