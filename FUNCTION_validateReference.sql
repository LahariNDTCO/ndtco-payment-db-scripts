SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[validateReference] (@ref varchar(30))
RETURNS int 
as 
BEGIN
    set @ref=REPLACE(@ref,' ','');
    declare @ret int = 0;
    if( isnumeric(@ref) = 1)
        BEGIN
             set @ret= iif(CONVERT(int,@ref)=0,1,0);
        end
    else 
        BEGIN
             set @ret= iif(len(@ref) != 0,0,1);
        END
    return @ret;
end

GO
