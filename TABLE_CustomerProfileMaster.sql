SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
CREATE TABLE [dbo].[CustomerProfileMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[accountNumber] [varchar](50) NOT NULL,
	[customerProfileId] [varchar](20) NOT NULL,
	[customerName] [varchar](50) NULL,
	[customerAddressLine1] [varchar](40) NULL,
	[customerAddressLine2] [varchar](40) NULL,
	[customerAddressLine3] [varchar](40) NULL,
	[customerAddressState] [varchar](2) NULL,
	[customerAddressCity] [varchar](30) NULL,
	[customerAddressZip] [varchar](10) NULL,
	[phoneNumber] [varchar](15) NULL,
	[emailAddress] [varchar](50) NULL,
	[paymentMethod] [varchar](20) NULL,
	[accountStatus] [bit] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
 CONSTRAINT [PK_CPMID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[customerProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[customerProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerProfileMaster] ADD  DEFAULT ((0)) FOR [accountStatus]
GO
