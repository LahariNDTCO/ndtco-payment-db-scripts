SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO
--============================================
-- created by pavan on 03072020
-- batchWise  list
--============================================
CREATE procedure [dbo].[getBatchNamesFromBatchNameList]
--declare
@FromDate varchar(30)=null,
@ToDate varchar(30)=null,
@getName varchar(50)=NULL

as 
begin 
    select bnl.ID batchId,bnl.batchName batchName,bnl.createdDate createdDate
    from batchNameList bnl where bnl.batchName like '%'+isnull(@getName,'')+'%' --and  len(@getName)>0
        and convert(date,BNL.createdDate,111) BETWEEN 
            ( case when isnull(@FromDate,'') = '' then convert(date,BNL.[createdDate],111) else convert(date,@FromDate,111) end)
                and 
            ( case when isnull(@ToDate,'') = '' then convert(date,BNL.[createdDate],111) else   convert(date,@ToDate,111) end)
end


GO
